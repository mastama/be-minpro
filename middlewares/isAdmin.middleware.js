const { Users } = require('../models');
const isAdmin = (req, res, next) => {
	const { is_admin } = req.headers;
	if (is_admin === 'AUTHORIZED') {
		next();
	} else {
		res.status(404).send({
			message: 'Not found',
		});
	}
};

module.exports = isAdmin;
