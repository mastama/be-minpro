const { Users } = require('../models');
const checkDuplicateNameOrEmail = (req, res, next) => {
	// Username
	Users.findOne({
		where: { name: req.body.name },
	}).then((user) => {
		if (user) {
			res.status(400).send({
				message: 'Failed! Username is already in use!',
			});
			return;
		}
		// Email
		Users.findOne({ where: { email: req.body.email } }).then((user) => {
			if (user) {
				res.status(400).send({
					message: 'Failed! Email is already in use!',
				});
				return;
			}
			next();
		});
	});
};
module.exports = checkDuplicateNameOrEmail;
