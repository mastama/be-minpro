const authorization = (req, res, next) => {
	try {
		const payload = req.user_login;
		const { id } = req.params;

		console.log(payload, '<< PAYLOAD');
		console.log(id);

		if (payload.id !== id) {
			res.status(401).json({
				message: 'You are unauthorized',
			});
			return;
		}
		next();
	} catch (error) {
		console.log(error);
	}
};

module.exports = authorization;
