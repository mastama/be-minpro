const authentication = require('./authentication.middleware');
const authorization = require('./authorization.middleware');
const isAdmin = require('./isAdmin.middleware');
const checkDuplicateNameOrEmail = require('./checkDuplicateNameOrEmail.middleware');
module.exports = {
	authentication,
	authorization,
	isAdmin,
	checkDuplicateNameOrEmail,
};
