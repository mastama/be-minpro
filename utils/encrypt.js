const bcrypt = require('bcrypt');

class Encryption {
	static encryptPwd(rawPassword) {
		try {
			const salt = bcrypt.genSaltSync(10);
			const hash = bcrypt.hashSync(rawPassword, salt);
			return hash;
		} catch (error) {
			return null;
		}
	}

	static isValidPwd(rawPassword, hash) {
		try {
			return bcrypt.compareSync(rawPassword, hash);
		} catch (error) {
			return null;
		}
	}
}

module.exports = Encryption;
