require('dotenv').config();

const jwt = require('jsonwebtoken');
class Token {
	static generateToken(payload) {
		return jwt.sign(payload, process.env.SECRET_KEY);
	}

	static decodeToken(token) {
		try {
			const verifiedToken = jwt.verify(token, process.env.SECRET_KEY);
			return !verifiedToken ? null : verifiedToken;
		} catch (error) {
			return null;
		}
	}
}

module.exports = Token;
