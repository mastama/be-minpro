"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "PaymentAccounts",
      [
        {
          id: "1",
          bank_name: "Bank BCA",
          account_number: "3751785066",
          recipient_name: "Sakara Wiyono",
          bank_logo: "http://localhost:3000/images/bank-logo/bank-bca.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "2",
          bank_name: "Bank BNI",
          account_number: "3751785066",
          recipient_name: "Sakara Wiyono",
          bank_logo: "http://localhost:3000/images/bank-logo/bank-bni.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "3",
          bank_name: "Bank BRI",
          account_number: "3751785066",
          recipient_name: "Sakara Wiyono",
          bank_logo: "http://localhost:3000/images/bank-logo/bank-bri.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "4",
          bank_name: "Bank Mandiri",
          account_number: "3751785066",
          recipient_name: "Sakara Wiyono",
          bank_logo: "http://localhost:3000/images/bank-logo/bank-mandiri.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
