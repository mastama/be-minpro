const express = require("express");
const app = express();
const fileUpload = require("express-fileupload");

app.get("/", (req, res) => {
  res.send("Hello world !");
});

app.use(express.static("public"));
app.use(fileUpload());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const routes = require("./routes");

require("dotenv").config();
app.use(routes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is listening on port, ${PORT}`);
});
