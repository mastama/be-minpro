const { AuthController } = require('../controllers');
const {
	checkDuplicateNameOrEmail,
	isAdmin,
	authorization,
	authentication,
} = require('../middlewares');
const router = require('express').Router();

router.get('/users/:id', AuthController.getUser);
router.put('/users/:id', AuthController.updateUser);

// 1.
// Route for user login and logout
router.post('/login', AuthController.login);
// TODO: FE harus simpen token setelah login ke local storage atau cookies dlu, 
// TODO: hbs tuh klo udh mw logout, pasangin tokennya ke headers
router.post('/logout', [authentication], AuthController.logout);

// 2.
// Route for register new user
//TODO: Need middleware -> Check if users exists with duplicate name or email validation middleware
router.post('/register', [checkDuplicateNameOrEmail], AuthController.register);

// 3.
// Routes for only admin can access
//TODO: Need middleware -> Check if user roles is admin
router.get('/users', [isAdmin], AuthController.getUsers);
router.delete('/users/:id', [isAdmin], AuthController.deleteUser);

module.exports = router;

// TODO: if user has signed in, still can access register page -> need to send 404
// TODO: if user has signed in, still can sign in again
