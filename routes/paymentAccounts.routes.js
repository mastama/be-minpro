const { PaymentAccountsController } = require("../controllers");
const router = require("express").Router();

// Ambil semua data informasi rekening pembayaran
router.get("/", PaymentAccountsController.getPaymentAccounts);

module.exports = router;
