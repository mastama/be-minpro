const { TransactionsController } = require("../controllers");
const router = require("express").Router();

// Buat transaksi baru
router.post("/", TransactionsController.createTransaction);

// Ambil semua data transaksi
router.get("/", TransactionsController.getAllTransactions);

// Ambil data transaksi berdasarkan event_id
router.get("/:event_id", TransactionsController.getTransaction);

// Update status pembayaran fullpayment atau downpayment berdasarkan fullpayment_transaction_id atau downpayment_transaction_id
router.put("/status", TransactionsController.updateStatusPayment);

// Upload bukti pembayaran fullpayment atau downpayment berdasarkan fullpayment_transaction_id atau downpayment_transaction_id
router.put("/proof-of-transfer", TransactionsController.proofOfTransfer);

module.exports = router;
