const authRoutes = require("./auth.routes");
const paymentAccountsRoutes = require("./paymentAccounts.routes");
const transactionsRoutes = require("./transactions.routes");
const router = require("express").Router();

router.use("/auth", authRoutes);
router.use("/api/v1/payment-accounts", paymentAccountsRoutes);
router.use("/api/v1/transactions", transactionsRoutes);

module.exports = router;
