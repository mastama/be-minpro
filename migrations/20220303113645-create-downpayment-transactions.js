"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("DownpaymentTransactions", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      payment_account_id: {
        type: Sequelize.STRING,
      },
      downpayment_status: {
        type: Sequelize.STRING,
      },
      transfer_proof_image: {
        type: Sequelize.STRING,
      },
      downpayment_time: {
        type: Sequelize.DATE,
      },
      downpayment_deadline: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("DownpaymentTransactions");
  },
};
