const { Users } = require('../models');
const { Token, Encrypt } = require('../utils');
const uuid = require('uuid');

class AuthController {
	static login = async (req, res) => {
		try {
			const { name, password } = req.body;
			const user = await Users.findOne({
				where: { name },
			});
			const comparePwd = user
				? Encrypt.isValidPwd(password, user.dataValues.password)
				: 0;
			if (!user || !comparePwd) {
				res.status(401).json({
					status: 401,
					message: 'Unauthorize',
				});
				return;
			}
			const access_token = Token.generateToken(user.dataValues);
			res.status(200).json({ session_token: access_token });
		} catch (error) {
			console.log(error);
			res.status(500).json({ error });
		}
	};
	static logout = async (req, res) => {
		try {
			res.status(200).json({
				status: 200,
				message: 'Success logout',
			});
		} catch (error) {
			console.log(error);
			res.status(500).json({ error });
		}
	};
	static register = async (req, res) => {
		try {
			const id = uuid.v4();
			let { name, email, agency_name, password } = req.body;
			if (!name || !email || !agency_name || !password) {
				res.status(200).json({
					status: 'Failed',
					message: 'Function required body field',
				});
				return;
			}

			const role = 'client';
			password = Encrypt.encryptPwd(password);

			const payload = {
				id,
				name,
				email,
				agency_name,
				password,
				role,
			};
			const newUser = await Users.create(payload);
			res.status(200).json({ data: newUser });
		} catch (error) {
			console.log(error);
			res.status(500).json({ error });
		}
	};
	static getUser = async (req, res) => {
		try {
			const { id } = req.params;
			const user = await Users.findOne({
				where: {
					id: id,
				},
			});
			if (!user) {
				res.status(200).json({
					message: `Find no user with id ${id}`,
				});
				return;
			}
			res.status(200).json({ data: user });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
	static updateUser = async (req, res) => {
		try {
			let { name, email, agency_name, password } = req.body;
			if (password) {
				password = Encrypt.encryptPwd(password);
			}
			const payload = {
				name,
				email,
				agency_name,
				password,
			};
			const { id } = req.params;
			const options = {
				where: {
					id,
				},
				returning: true,
			};
			const updated = await Users.update(payload, options);
			if (!updated) {
				res.status(200).json({
					message: `Find no user with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: updated,
			});
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
	static getUsers = async (req, res) => {
		try {
			const users = await Users.findAll();
			res.status(200).json({ data: users });
		} catch (error) {
			console.log(error);
		}
	};
	static deleteUser = async (req, res) => {
		try {
			const { id } = req.params;
			const deleted = await Users.destroy({
				where: {
					id,
				},
				returning: true,
			});
			if (!deleted) {
				res.status(200).json({
					message: `Find no user with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: deleted,
			});
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
}

module.exports = AuthController;
