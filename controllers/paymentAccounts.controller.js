const { PaymentAccounts } = require("../models");

class PaymentAccountsController {
  static getPaymentAccounts = async (req, res) => {
    try {
      const paymentAccounts = await PaymentAccounts.findAll();
      res.send({
        status: 200,
        message: "Success get payment accounts",
        results: paymentAccounts,
      });
    } catch (error) {
      res.send({
        code: 500,
        message: error,
      });
    }
  };
}

module.exports = PaymentAccountsController;
