const AuthController = require("./auth.controller");
const PaymentAccountsController = require("./paymentAccounts.controller");
const TransactionsController = require("./transactions.controller");

module.exports = {
  AuthController,
  PaymentAccountsController,
  TransactionsController,
};
