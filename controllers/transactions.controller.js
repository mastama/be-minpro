const {
  Transactions,
  FullpaymentTransactions,
  DownpaymentTransactions,
  PaymentAccounts,
} = require("../models");
const path = require("path");
const uuid = require("uuid");

class TransactionsController {
  static createTransaction = async (req, res) => {
    try {
      const {
        event_id,
        total_price,
        downpayment_deadline,
        fullpayment_deadline,
      } = req.body;
      if (
        !event_id ||
        !total_price ||
        !downpayment_deadline ||
        !fullpayment_deadline
      ) {
        res.send({
          code: 400,
          message: "Please fill all the fields",
        });
      } else {
        const findTransactionByEventId = await Transactions.findOne({
          where: { event_id },
        });
        if (findTransactionByEventId) {
          res.send({
            code: 409,
            message: "Event already have a transaction",
          });
        } else {
          const transaction_id = uuid.v4();
          const fullpayment_transaction_id = uuid.v4();
          const downpayment_transaction_id = uuid.v4();
          await FullpaymentTransactions.create({
            id: fullpayment_transaction_id,
            payment_account_id: null,
            fullpayment_status: "unpaid",
            transfer_proof_image: null,
            fullpayment_time: null,
            fullpayment_deadline,
          });
          await DownpaymentTransactions.create({
            id: downpayment_transaction_id,
            payment_account_id: null,
            downpayment_status: "unpaid",
            transfer_proof_image: null,
            downpayment_time: null,
            downpayment_deadline,
          });
          await Transactions.create({
            id: transaction_id,
            event_id,
            fullpayment_transaction_id,
            downpayment_transaction_id,
            total_price,
          });
          res.send({
            status: 200,
            message: "Success create transaction",
          });
        }
      }
    } catch (error) {
      res.send({
        code: 500,
        message: error,
      });
    }
  };

  static getAllTransactions = async (req, res) => {
    try {
      const transactions = await Transactions.findAll({
        attributes: {
          exclude: ["fullpayment_transaction_id", "downpayment_transaction_id"],
        },
        include: [
          {
            model: FullpaymentTransactions,
            as: "fullpayment_transaction",
            attributes: {
              exclude: ["payment_account_id"],
            },
            include: [
              {
                model: PaymentAccounts,
                as: "payment_account",
              },
            ],
          },
          {
            model: DownpaymentTransactions,
            as: "downpayment_transaction",
            attributes: {
              exclude: ["payment_account_id"],
            },
            include: [
              {
                model: PaymentAccounts,
                as: "payment_account",
              },
            ],
          },
        ],
      });
      res.send({
        status: 200,
        message: "Success get all transactions",
        results: transactions,
      });
    } catch (error) {
      res.send({
        code: 500,
        message: error,
      });
    }
  };

  static getTransaction = async (req, res) => {
    try {
      const { event_id } = req.params;
      const transaction = await Transactions.findOne({
        where: { event_id },
        attributes: {
          exclude: ["fullpayment_transaction_id", "downpayment_transaction_id"],
        },
        include: [
          {
            model: FullpaymentTransactions,
            as: "fullpayment_transaction",
            attributes: {
              exclude: ["payment_account_id"],
            },
            include: [
              {
                model: PaymentAccounts,
                as: "payment_account",
              },
            ],
          },
          {
            model: DownpaymentTransactions,
            as: "downpayment_transaction",
            attributes: {
              exclude: ["payment_account_id"],
            },
            include: [
              {
                model: PaymentAccounts,
                as: "payment_account",
              },
            ],
          },
        ],
      });
      res.send({
        status: 200,
        message: "Success get transaction",
        results: transaction,
      });
    } catch (error) {
      res.send({
        code: 500,
        message: error,
      });
    }
  };

  static updateStatusPayment = async (req, res) => {
    try {
      const { fullpayment_transaction_id, downpayment_transaction_id, status } =
        req.body;
      if (downpayment_transaction_id && status) {
        await DownpaymentTransactions.update(
          { downpayment_status: status },
          { where: { id: downpayment_transaction_id } }
        );
        res.send({
          status: 200,
          message: "Success update status down payment",
        });
      } else if (fullpayment_transaction_id && status) {
        await FullpaymentTransactions.update(
          { fullpayment_status: status },
          { where: { id: fullpayment_transaction_id } }
        );
        res.send({
          status: 200,
          message: "Success update status full payment",
        });
      } else {
        res.send({
          code: 400,
          message: "Please fill all the fields",
        });
      }
    } catch (error) {
      res.send({
        code: 500,
        message: error,
      });
    }
  };

  static proofOfTransfer = async (req, res) => {
    try {
      const {
        downpayment_transaction_id,
        fullpayment_transaction_id,
        payment_account_id,
      } = req.body;
      const transferProofImage = req.files.transfer_proof_image;
      if (!req.body || !transferProofImage) {
        res.send({
          code: 400,
          message: "Function required req body and file",
        });
      } else {
        const newNameImage = `${uuid.v4()}.${transferProofImage.mimetype.replace(
          "image/",
          ""
        )}`;
        const dirName = path.join(
          __dirname,
          "../public/images/transfer-proof/"
        );
        const pathImage = path.join(
          req.get("host") + "/images/transfer-proof/" + newNameImage
        );
        transferProofImage.mv(dirName + newNameImage, async (err) => {
          if (err) {
            res.send({
              code: 500,
              message: "Failed to upload image",
            });
          } else {
            if (downpayment_transaction_id) {
              const downpayment_time = new Date();
              await DownpaymentTransactions.update(
                {
                  payment_account_id,
                  transfer_proof_image: pathImage,
                  downpayment_time,
                },
                {
                  where: { id: downpayment_transaction_id },
                }
              );
              res.send({
                status: 200,
                message: "Success update downpayment transaction",
              });
            } else if (fullpayment_transaction_id) {
              const fullpayment_time = new Date();
              await FullpaymentTransactions.update(
                {
                  payment_account_id,
                  transfer_proof_image: pathImage,
                  fullpayment_time,
                },
                {
                  where: { id: fullpayment_transaction_id },
                }
              );
              res.send({
                status: 200,
                message: "Success update fullpayment transaction",
              });
            }
          }
        });
      }
    } catch (error) {
      res.send({
        code: 500,
        message: error,
      });
    }
  };
}

module.exports = TransactionsController;
