"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class FullpaymentTransactions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      FullpaymentTransactions.hasOne(models.Transactions, {
        foreignKey: "fullpayment_transaction_id",
        as: "transaction",
      });
      FullpaymentTransactions.belongsTo(models.PaymentAccounts, {
        foreignKey: "payment_account_id",
        as: "payment_account",
      });
    }
  }
  FullpaymentTransactions.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.STRING,
      },
      payment_account_id: DataTypes.STRING,
      fullpayment_status: DataTypes.STRING,
      transfer_proof_image: DataTypes.STRING,
      fullpayment_time: DataTypes.DATE,
      fullpayment_deadline: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "FullpaymentTransactions",
    }
  );
  return FullpaymentTransactions;
};
