"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Transactions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Transactions.belongsTo(models.FullpaymentTransactions, {
        foreignKey: "fullpayment_transaction_id",
        as: "fullpayment_transaction",
      });
      Transactions.belongsTo(models.DownpaymentTransactions, {
        foreignKey: "downpayment_transaction_id",
        as: "downpayment_transaction",
      });
    }
  }
  Transactions.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.STRING,
      },
      event_id: DataTypes.STRING,
      fullpayment_transaction_id: DataTypes.STRING,
      downpayment_transaction_id: DataTypes.STRING,
      total_price: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Transactions",
    }
  );
  return Transactions;
};
