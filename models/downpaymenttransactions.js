"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class DownpaymentTransactions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      DownpaymentTransactions.hasOne(models.Transactions, {
        foreignKey: "downpayment_transaction_id",
        as: "transaction",
      });
      DownpaymentTransactions.belongsTo(models.PaymentAccounts, {
        foreignKey: "payment_account_id",
        as: "payment_account",
      });
    }
  }
  DownpaymentTransactions.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.STRING,
      },
      payment_account_id: DataTypes.STRING,
      downpayment_status: DataTypes.STRING,
      transfer_proof_image: DataTypes.STRING,
      downpayment_time: DataTypes.DATE,
      downpayment_deadline: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "DownpaymentTransactions",
    }
  );
  return DownpaymentTransactions;
};
