"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PaymentAccounts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PaymentAccounts.hasOne(models.FullpaymentTransactions, {
        foreignKey: "payment_account_id",
        as: "fullpayment_transaction",
      });
      PaymentAccounts.hasOne(models.DownpaymentTransactions, {
        foreignKey: "payment_account_id",
        as: "downpayment_transaction",
      });
    }
  }
  PaymentAccounts.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.STRING,
      },
      bank_name: DataTypes.STRING,
      account_number: DataTypes.STRING,
      recipient_name: DataTypes.STRING,
      bank_logo: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "PaymentAccounts",
    }
  );
  return PaymentAccounts;
};
